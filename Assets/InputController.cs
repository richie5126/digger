﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(RigidbodyController))]
public class InputController : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField] string walkParamName;
    Vector3 ipos;
    public void SetFloat(float val)
    {
        GetComponent<Animator>().SetFloat(walkParamName, val);
    }
    void Start()
    {
        ipos = transform.position;
    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<RigidbodyController>().Move(Input.GetAxisRaw("Horizontal"), false, Input.GetButton("Jump"));
    }
    public void Die()
    {
        transform.position = ipos;
    }
}
