﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(UnityEngine.UI.Text))]
public class readyup : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public void ConfirmIsReady(Character c)
    {
        
        GetComponent<UnityEngine.UI.Text>().text = "O";
        GetComponent<UnityEngine.UI.Text>().color = Color.green;
        c.IsReady = true;

    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
