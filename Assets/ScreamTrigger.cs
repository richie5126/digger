﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreamTrigger : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] ParticleSystem Explosion;
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        Debug.Log("ow!");
        if(other.attachedRigidbody != null)
        {
            SendMessageUpwards("Scream");
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.attachedRigidbody != null)
        {
            Instantiate(Explosion, transform.position, transform.rotation);
            GameManager.Instance.CharacterLost(GetComponentInParent<Character>());
            Destroy(transform.root.gameObject);
        }

    }
}
