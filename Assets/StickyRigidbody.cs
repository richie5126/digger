﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

[RequireComponent(typeof(Rigidbody2D))]
public class StickyRigidbody : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    // Start is called before the first frame update
    void Start()
    {
        GetComponent<Cinemachine.CinemachineImpulseSource>()?.GenerateImpulse();
    }

    // Update is called once per frame
    bool _dragged;
    void FixedUpdate()
    {
        if(_dragged)
        {
            GetComponent<Rigidbody2D>().velocity =
                (Camera.main.ScreenToWorldPoint(Input.mousePosition)
                - transform.position) * 30.0f;

        }
    }
    void Update()
    {
        
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        //if(collision.collider.attachedRigidbody != null)
        //{
        //    transform.SetParent(collision.collider.transform, true);
        //    transform.GetComponent<Rigidbody2D>().isKinematic = true;
        //}
        if(collision.relativeVelocity.sqrMagnitude > 2.0f)
        GetComponent<AudioSource>()?.Play();
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        _dragged = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        _dragged = false;
    }
}
