﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Character : MonoBehaviour
{
    public int LocalScore = 0;
    public int PlayerNumber;
    public GameObject BrickPrefab;
    public Transform BrickTarget;

    public bool IsReady = false;

    Vector2 targetDirection = Vector2.right;
    [SerializeField] GameObject targetIndicator;


    [SerializeField] int imagePipCount = 1;
    public UnityEngine.UI.Image roundMeter;
    string Horizontal => (PlayerNumber > 1 ? "Horizontal" : "Horizontal2");
    string Vertical => (PlayerNumber > 1 ? "Vertical" : "Vertical2");

    // Start is called before the first frame update
    void Start()
    {
        GenerateRandomDirection();
    }
    void GenerateRandomDirection()
    {

        targetDirection = (Random.Range(0, 2) != 0 ?

        (Random.Range(0, 2) != 0 ? Vector2.up : Vector2.down) :


        (Random.Range(0, 2) != 0 ? Vector2.left : Vector2.right));


        float angle = Mathf.Atan2(targetDirection.y, targetDirection.x) * Mathf.Rad2Deg;
        targetIndicator.transform.rotation = Quaternion.AngleAxis(angle + 90f, -Vector3.forward);
    }
    [SerializeField] string animatorAttackName;
    [SerializeField] string animatorStunnedName;
    [SerializeField] string animatorScreamName;

    [SerializeField] ParticleSystem DirtFX;
    [SerializeField] readyup ReadyUp;

    // Update is called once per frame
    bool isPressed;
    public void Scream()
    {
        GetComponent<Animator>().SetTrigger(animatorScreamName);
    }

    [SerializeField] AudioClip DigClip;
    [SerializeField] AudioClip StunClip;
    public void PlayOneShot(Object clip)
    {

        GetComponent<AudioSource>()?.PlayOneShot(clip as AudioClip);
    }
    public void PlayDig()
    {
        GetComponent<AudioSource>()?.PlayOneShot(DigClip);
    }
    public void PlayStun()
    {
        GetComponent<AudioSource>()?.PlayOneShot(StunClip);
    }
    void PlayScreamAudio()
    {
        if(!GetComponent<AudioSource>().isPlaying)
        GetComponent<AudioSource>()?.Play();
    }
    public string idleStateName;

    public void AddScore()
    {

        if (!GameManager.Instance.IsStarted) return;
        ++LocalScore;
    }
    void Update()
    {
        if(!IsReady && (Input.GetAxisRaw(Horizontal) != 0 || Input.GetAxisRaw(Vertical) != 0))
        {

            if (ReadyUp && ReadyUp.gameObject.activeInHierarchy)
            {
                DirtFX?.Emit(15);
                GetComponent<Animator>().SetTrigger(animatorAttackName);
                ReadyUp.ConfirmIsReady(this);
            }
            return;
        }
        //score.text = LocalScore.ToString();
        roundMeter.fillAmount = LocalScore / (float)imagePipCount;
        if ((Input.GetAxisRaw(Horizontal) != 0 || Input.GetAxisRaw(Vertical) != 0) && !isPressed
            && GetComponent<Animator>().GetCurrentAnimatorStateInfo(0).IsName(idleStateName))
        {
            isPressed = true;

            if (Vector2.Dot(targetDirection, new Vector2(Input.GetAxisRaw(Horizontal),
                Input.GetAxisRaw(Vertical))) > 0)
            {
                DirtFX?.Emit(15);
                GenerateRandomDirection();
                GetComponent<Animator>().SetTrigger(animatorAttackName);
            }
            else GetComponent<Animator>().SetTrigger(animatorStunnedName);
        }
        else if ((Input.GetAxisRaw(Horizontal).Equals(0) && Input.GetAxisRaw(Vertical).Equals(0)))
            isPressed = false;
        
    }
}
