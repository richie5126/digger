﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using System.Threading.Tasks;
public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField] List<Character> characters = new List<Character>();
    [SerializeField] List<AudioSource> sources = new List<AudioSource>();
    AudioSource _clipSource { get { return sources[0]; } }
    [SerializeField] UnityEngine.UI.Image fill;
    [SerializeField] GameObject WinnerPrefab;
    [SerializeField] GameObject TiedBrickPrefab;
    Dictionary<Character, int> scores = new Dictionary<Character, int>();
    [SerializeField] UnityEngine.UI.Text CountDownText;
    [SerializeField] UnityEngine.Canvas countdownCanvas;



    int currentRound = 0;
    private bool isStarted;
    public bool IsStarted { get { return isStarted; } }
    public IEnumerator MainLoop()
    {
        countdownCanvas.gameObject.SetActive(true);

        while (true)
        {
            bool all = true;
            foreach(var pair in scores)
            {
                if (!pair.Key.IsReady) all = false;
            }
            if (all) break;
            yield return new WaitForSeconds(2f);
        }
        int seconds = 3;
        while (seconds > 0)
        {
            CountDownText.text = "" + seconds;
            _clipSource.PlayOneShot(_countDownClip);
            --seconds;
            yield return new WaitForSeconds(1f);
        }
        CountDownText.text = "Repair!";
        _clipSource.PlayOneShot(_countedDownClip);
        yield return new WaitForSeconds(1f);

        countdownCanvas.gameObject.SetActive(false);

        isStarted = true;
        while (true)
        {
            _clipSource.Play();
            for(int i = 0; i < sources.Count; ++i)
            {
                if(!sources[i].isPlaying) sources[i].Play();
                sources[i].time = sources[0].time;

                if (currentRound >= i * 4)
                    sources[i].volume = 1;
            }


            while (_clipSource.isPlaying)
            {
                fill.fillAmount = (_clipSource.clip.length - _clipSource.time) / _clipSource.clip.length;
                yield return null;
            }
            if(scores.Count <= 1)
            {
                foreach(var source in sources)
                {
                    source.Stop();
                }
                Debug.Log("Winner!");
                _clipSource.pitch = 1;
                _clipSource.PlayOneShot(_winFanfare);
                ShowWinner(scores.FirstOrDefault().Key);
                break;
            }
            foreach (var source in sources)
            {
                source.time = 0;
                source.pitch += 0.1f;
            }
            GenerateBrickForWinner();
            ++currentRound;
        }
    }
    [SerializeField] AudioClip _winFanfare;
    [SerializeField] AudioClip _countDownClip;
    [SerializeField] AudioClip _countedDownClip;
    public void ShowWinner(Character c)
    {
        var g = Instantiate(WinnerPrefab);
        g.GetComponentInChildren<UnityEngine.UI.Text>().text = "Player " + c.PlayerNumber + " wins!";

    }
    public void CharacterLost(Character c)
    {
        scores.Remove(c);
        Debug.Log(c.PlayerNumber + " Is Out!");

    }
    public void StartGame()
    {
        StartCoroutine(MainLoop());
    }
    void Start()
    {
        foreach (var c in characters)
        {
            scores.Add(c, 0);
        }
        //InvokeRepeating("GenerateBrickForWinner", 2.0f, 2.0f);
    }
    static GameManager _inst;
    private void Awake()
    {
        if (_inst != null) Destroy(this);
        else _inst = this;
    }
    public static GameManager Instance { get { return _inst; } }
    public void AddPoints(Character c)
    {
        c.LocalScore++;
    }
    [SerializeField] float winDelta = 0.1f;
    public void GenerateBrickForWinner()
    {
        Character roundwinner = null;
        int max = 0;
        int count = 0;
        foreach(var pair in scores)
        {
            if(pair.Key.LocalScore == max)
            {
                ++count;
            }
            else if(pair.Key.LocalScore > max)
            {
                roundwinner = pair.Key;
                max = pair.Key.LocalScore;
                count = 1;
            }
        }

        if (count < 2)
        {
            transform.position = Vector3.MoveTowards(transform.position, roundwinner.BrickTarget.position, winDelta);
            Instantiate(roundwinner.BrickPrefab, transform.position, Quaternion.identity);
        }
        else
        {
            Instantiate(TiedBrickPrefab, transform.position, Quaternion.identity);
        }
        foreach (var pair in scores)
        {
            pair.Key.LocalScore = 0;
        }
    }
}
